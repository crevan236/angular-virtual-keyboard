import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  element: FormControl;
  show = true;
  //
  ngOnInit(): void {
    this.element = new FormControl('');
    this.element.valueChanges.subscribe(val => console.log(val));
  }
  onToggle(): void {
    this.show = !this.show;
  }
}
