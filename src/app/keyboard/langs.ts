export interface Langs {
    value: Lang[];
}
export interface Lang {
    id: string;
    title: string;
    letters: string[][];
    symbols: string[][];
}
