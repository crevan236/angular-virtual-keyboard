import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';

import { Subject } from 'rxjs/Subject';

import { Langs, Lang } from '../langs';
import { DefaultSettings } from '../default-langs/key-map';

@Component({
  selector: 'app-virtual-keyboard',
  templateUrl: './virtual-keyboard.component.html',
  styleUrls: ['./virtual-keyboard.component.css']
})
export class VirtualKeyboardComponent implements OnInit {
  @Input('formElement') formElement: AbstractControl;
  @Input()
  get visible() {
    return this.visibleValue;
  }
  @Output() visibleChange = new EventEmitter();
  set visible(val) {
    this.visibleValue = val;
    this.visibleChange.emit(this.visibleValue);
  }
  //
  visibleValue = false;
  //
  settings: Langs;
  currentLang = 0;
  keyMap: string[][];
  symMap: string[][];
  //
  lang = new FormControl(this.currentLang);
  //
  keyboadChanges = new Subject();
  //
  currentValue: string;
  //
  uppercase: number;
  isShift: number;
  isAlt: number;
  //
  isKeyboardDraggable = true;
  //
  constructor() {
    this.keyboadChanges.subscribe(
      val => {
        if (this.formElement) {
          let temp = this.formElement.value;
          temp += val;
          this.formElement.setValue(temp);
        } else {
          this.currentValue += val;
        }
      }
    );
    this.lang.valueChanges.subscribe(
      val => this.changeLang(val)
    );
    //
    this.isKeyboardDraggable = window.innerWidth > 764 ? true : false;
  }
  //
  ngOnInit() {
    this.settings = new DefaultSettings();
    this.keyMap = this.settings.value[this.currentLang].letters;
    this.symMap = this.settings.value[this.currentLang].symbols;
    //
    this.currentValue = '';
    this.uppercase = 0;
    this.isShift = 0;
    this.isAlt = 0;
  }
  //
  addLetter(letter: string): void {
    this.keyboadChanges.next(letter);
  }
  toggleUppercase(): void {
    this.uppercase = this.uppercase === 0 ? 1 : 0;
  }
  toggleShift(): void {
    this.isShift = this.isShift === 0 ? 1 : 0;
  }
  toggleAlt(): void {
    this.isAlt = this.isAlt === 0 ? 2 : 0;
  }
  removeAll(): void {
    if (this.formElement) {
      this.formElement.setValue('');
    } else {
      this.currentValue = '';
    }
  }
  removeLast(): void {
    let temp: string;
    if (this.formElement) {
      temp = this.formElement.value;
      temp = temp.split('').slice(0, this.currentValue.length - 1).join('');
      this.formElement.setValue(temp);
    } else {
      temp = this.currentValue;
      temp = temp.split('').splice(0, this.currentValue.length - 1).join('');
      this.currentValue = '';
      this.keyboadChanges.next(temp);
    }
  }
  onHide(): void {
    this.visible = false;
  }
  changeLang(nr: number): void {
    this.currentLang = nr;
    this.keyMap = this.settings.value[this.currentLang].letters;
    this.symMap = this.settings.value[this.currentLang].symbols;
  }
}
