import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { VirtualKeyboardComponent } from './virtual-keyboard/virtual-keyboard.component';
import { AngularDraggableModule } from 'angular2-draggable';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AngularDraggableModule
],
  declarations: [
    VirtualKeyboardComponent
  ],
  exports: [
    VirtualKeyboardComponent
  ]
})
export class KeyboardModule {}
